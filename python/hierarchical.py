import numpy as np
from matplotlib import pyplot as plt
import sys

'''
======================================================================

       Hierarchical sampler for 2d Ising model

======================================================================
'''

class LocalGibbsDistribution(object):
    '''Distribution for a local spin s = {-1,+1}, given the sum of all four 
    surrounding spins \Sigma. Since the Ising Hamiltonian is
    -\sum_{<i,j>} s_i*s_j, this probability distribution is given by

    P(s|\Sigma) = N^{-1}*exp[-\beta*(-s*\Sigma)]
                = \sigma(2*\beta*s*\Sigma)

    where \sigma(z) = 1/(1+e^{-z}) is the sigmoid function

    It is also possible to 'bias' the distribution such that the most likely
    state is chosen with probability q, and with probability 1-q a choice is
    made according to the above distribution.

    :arg beta: coupling constant \beta
    :arg q_bias: bias-parameter
    '''

    def __init__(self,beta,q_bias=0):
        self.beta = beta
        self.q_bias = q_bias

    def evaluate(self,spin,Sigma):
        '''Evaluate probability for given s and Sigma

        :arg spin: value of spin
        :arg Sigma: sum of four surrounding spins
        '''
        if spin < 0:
            return 1-self.evaluate(-spin,Sigma)
        else:
            rho = 1./(1.+np.exp(-2.*self.beta*Sigma))
            if Sigma > 0:
                return self.q_bias + (1-self.q_bias)*rho
            elif Sigma < 0:
                return (1-self.q_bias)*rho
            else:
                return 0.5

    def draw(self,Sigma):
        '''Draw a sample from the distribution for a given Sigma
        
        :arg Sigma: sum of four surrounding spins
        '''
        p_up = self.evaluate(+1,Sigma)
        xi = np.random.uniform(0.0,1.0)
        return +1 if (xi < p_up) else -1

class Sampler(object):
    '''Abstract sampler base class'''

    def __init__(self):
        self.n_samples = 0
        self.n_accept = 0

    def draw(self,config):
        '''Draw new sample [NOT IMPLEMENTED IN BASE CLASS]'''
        raise NotImplementedError
    
    def acceptance_rate(self):
        '''Return overall acceptance rate'''
        if (self.n_samples > 0):
            return self.n_accept/self.n_samples
        else:
            return 0
        
    def reset_statistics(self):
        '''Reset counters for number of (accepted) samples'''
        self.n_accept = 0
        self.n_samples = 0

class GibbsSampler(Sampler):
    '''Gibbs sampler

    :arg beta: coupling constant beta
    :arg n_iter: number of iterations to be carried out in each draw
    '''
    def __init__(self,beta,n_iter=1):
        super().__init__()
        self.beta = beta
        self.n_iter = n_iter
        self.local_dist = LocalGibbsDistribution(beta)
        
    def draw(self,config):
        '''Carry out n_iter iterations of the Gibbs sampler

        :arg config: spin configuration to act on
        '''
        N_lat = config.shape[0]
        linear_indices = np.asarray(range(N_lat**2),dtype=int)
        for iter in range(self.n_iter):
            # Shuffle lattice indices
            np.random.shuffle(linear_indices)
            lattice_sites = [(ell//N_lat,ell%N_lat) for ell in linear_indices]
            for (i,j) in lattice_sites:
                Sigma = config[(i+1)%N_lat,j] \
                      + config[(i-1)%N_lat,j] \
                      + config[i,(j+1)%N_lat] \
                      + config[i,(j-1)%N_lat]
                config[i,j] = self.local_dist.draw(Sigma)

class HierarchicalSampler(Sampler):
    '''Hierarchical sampler

    :arg n_level: number of levels (1 = single-level method)
    :arg beta: coupling constant beta
    :arg n_iter_gibbs: number of iterations of coarse level Gibbs sampler
    :arg bias: bias for blue/red fill-in 
    '''
    def __init__(self,n_level,beta,n_iter_gibbs=1,q_bias=0):
        super().__init__()
        self.n_level = n_level
        self.beta = beta
        self.beta_coarse = beta
        self.local_dist = LocalGibbsDistribution(self.beta,q_bias=q_bias)
        self.coarse_gibbs_sampler = GibbsSampler(self.beta_coarse,n_iter_gibbs)
        self.gibbs_sampler = GibbsSampler(self.beta,n_iter_gibbs)

    def blue_sites(self,N_lat):
        '''Return the blue sites at tuples (i,j) for a given lattice size

        :arg N_lat: lattice size
        '''
        return [(i,j) for i in range(1,N_lat,2) for j in range(1,N_lat,2)]

    def red_sites(self,N_lat):
        '''Return the red sites at tuples (i,j) for a given lattice size

        :arg N_lat: lattice size
        '''
        return [(i,j) for i in range(0,N_lat,2) for j in range(1,N_lat,2)] \
             + [(i,j) for i in range(1,N_lat,2) for j in range(0,N_lat,2)]

    def fillin_bluered(self,config):
        '''Fill in the blue and red spins for a given configuration, assuming
        the coarse levels spin at the sites with indices (2*i,2*j) have
        already been sampled.
        
        :arg config: configuration to work on
        '''
        N_lat = config.shape[0]
        '''Fill in the blue and red spins'''
        for (i,j) in self.blue_sites(N_lat):
            Sigma = config[(i+1)%N_lat,(j+1)%N_lat] \
                  + config[(i-1)%N_lat,(j+1)%N_lat] \
                  + config[(i+1)%N_lat,(j-1)%N_lat] \
                  + config[(i-1)%N_lat,(j-1)%N_lat]
            config[i,j] = self.local_dist.draw(Sigma)
        for (i,j) in self.red_sites(N_lat):
            Sigma = config[(i+1)%N_lat,j] \
                  + config[(i-1)%N_lat,j] \
                  + config[i,(j+1)%N_lat] \
                  + config[i,(j-1)%N_lat]
            config[i,j] = self.local_dist.draw(Sigma)

    def hamiltonian(self,config):
        '''Evaluate the Hamiltonian for a given configuration
        
        :arg config: spin configuration to evaluate
        '''
        H_ising = 0.0
        N_lat = config.shape[0]
        for i in range(N_lat):
            for j in range(N_lat):
                Sigma = config[(i+1)%N_lat,j] \
                      + config[(i-1)%N_lat,j] \
                      + config[i,(j+1)%N_lat] \
                      + config[i,(j-1)%N_lat]
                H_ising += -0.5*config[i,j]*Sigma
        return H_ising
                
    def compute_dS(self,config,old_config):
        ''' Compute difference in Action between current (=proposal) and
        old (=previous) configuration.

        :arg config: current spin configuration (proposal)
        :arg config_old: previous spin configuration
        '''
        N_lat = config.shape[0]
        # difference in action on coarse level
        dS_coarse = self.beta_coarse*(self.hamiltonian(old_config[::2,::2])-self.hamiltonian(config[::2,::2]))
        # difference in action due to blue sites
        dS_blue = 0.0
        for (i,j) in self.blue_sites(N_lat):
            Sigma_old = old_config[(i+1)%N_lat,(j+1)%N_lat] \
                      + old_config[(i-1)%N_lat,(j+1)%N_lat] \
                      + old_config[(i+1)%N_lat,(j-1)%N_lat] \
                      + old_config[(i-1)%N_lat,(j-1)%N_lat]
            Sigma = config[(i+1)%N_lat,(j+1)%N_lat] \
                  + config[(i-1)%N_lat,(j+1)%N_lat] \
                  + config[(i+1)%N_lat,(j-1)%N_lat] \
                  + config[(i-1)%N_lat,(j-1)%N_lat]
            p_old = self.local_dist.evaluate(old_config[i,j],Sigma_old)
            p = self.local_dist.evaluate(config[i,j],Sigma)
            dS_blue -= np.log(p_old) - np.log(p)
            
        # difference in action due to red sites
        dS_red = 0.0
        for (i,j) in self.red_sites(N_lat):
            Sigma = config[(i+1)%N_lat,j] \
                  + config[(i-1)%N_lat,j] \
                  + config[i,(j+1)%N_lat] \
                  + config[i,(j-1)%N_lat]
            Sigma_old = old_config[(i+1)%N_lat,j] \
                      + old_config[(i-1)%N_lat,j] \
                      + old_config[i,(j+1)%N_lat] \
                      + old_config[i,(j-1)%N_lat]
            p_old = self.local_dist.evaluate(old_config[i,j],Sigma_old)
            p = self.local_dist.evaluate(config[i,j],Sigma)
            dS_red -= np.log(p_old) - np.log(p)
            
        # difference in action on fine level
        dS_fine = self.beta*(self.hamiltonian(config)-self.hamiltonian(old_config))
        return dS_coarse + dS_blue + dS_red + dS_fine      
                
    def _draw_recursive(self,config,level):
        '''Recursive helper function for sampling on a specific level

        :arg config: spin configuration
        :arg level: level (0=coarsest)
        '''
        old_config = config.copy()
        if level == 0:
            # Gibbs sampler on coarsest level
            self.coarse_gibbs_sampler.draw(config)
            return True
        else:
            # Draw coarse spins
            accept = self._draw_recursive(config[::2,::2],level-1)
            if (not accept):
                return False
            else:
                # Fill in blue and red spins
                self.fillin_bluered(config)
                # Compute change in action
                dS = self.compute_dS(config,old_config)
                # Metropolis accept/reject step
                if dS < 0:
                    return True
                else:
                    xi = np.random.uniform(0.0,1.0)
                    return (xi < np.exp(-dS))
            
        return config, accept
        
    def draw(self,config):
        '''Generate a new Markov-chain sample, the current configuration

        :arg config: current spin configuration:
        '''
        old_config = config.copy()
        accept = self._draw_recursive(config,self.n_level-1)
        if not accept:
            # Only copy over old configuration if proposal has not been accepted
            config[:,:] = old_config[:,:]
        else:
            self.n_accept += 1
        self.n_samples += 1
        
################################################################
# M A I N
################################################################
if (__name__ == '__main__'):
    beta_c = 0.5 * np.log(1.0+2.0**0.5) # Critical coupling
    beta = beta_c
    N_lat = 32 # Lattice size
    n_level = 3 # Number of levels
    n_burnin = 1000 # Number of burnin samples
    n_samples = 1000 # Number of samples
    n_iter_gibbs = 10 # Number of iterations in Gibbs sampler
    q_bias = 0.0
    print ('beta = ',beta)
    print ('N_lat = ',N_lat)
    print ('n_level = ',n_level)
    print ('q_bias = ',q_bias)
    np.random.seed(251257)
    # Create a new configuration, start with all spins in the up state
    config = np.ones((N_lat,N_lat))
    # Sampler
    sampler = HierarchicalSampler(n_level,beta,
                                  n_iter_gibbs=n_iter_gibbs,
                                  q_bias=q_bias)
    gibbs_sampler = GibbsSampler(beta,n_iter=1)
    # Burn in
    for n in range(n_burnin):
        sampler.draw(config)
    sampler.reset_statistics()
    # Sampling of susceptibility
    mu2 = 0.0
    mu4 = 0.0
    trajectory = np.zeros(n_samples)
    for n in range(n_samples):
        sampler.draw(config)
        # Compute magnetisation
        Q = np.mean(config)
        mu2 += (Q**2-mu2)/(n+1)
        mu4 += (Q**4-mu4)/(n+1)
        trajectory[n] = beta*Q**2
    # Save QoI for all samples to trajectory file
    np.save('trajectory.npy',trajectory)
    # Compute topological susceptibility
    chi_t = beta * mu2
    dchi_t = beta*np.sqrt((mu4-mu2**2)/(n_samples-1))
    # Print out results
    print ('chi_t/N^2 = '+('%8.5f' % chi_t)+' +/- '+('%8.5f' % dchi_t))
    print ('acceptance rate = ',('%6.2f' % (100*sampler.acceptance_rate())+' %'))

