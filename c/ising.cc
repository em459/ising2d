#include <iostream>
#include <random>
#include <cmath>
#include <chrono>
#include <stdio.h>

// Max 2d index pair (i,j) to a linear index
#define LIDX(n,i,j) (n)*((j)%n)+((i)%n)

// Problem size
int n;
// Coupling constant (inverse temperature) beta
double beta;
// Number of samples
const size_t n_sample = 100000;
// Number of burn-in samples
const size_t n_burnin = 100000;
// External field strength
const double h=0.0;
// Array holding the probabilities for spin-up depending on
// number of direct-neighbour up-spins
double p_gibbs[5];
// Seed for RNG
const int seed = 16247817;
// Random number engine
std::mt19937  engine(seed);
// Uniform distribution for Gibbs sampler
std::uniform_real_distribution<double> uniform_dist(0.0, 1.0);

/* Single (lexicographic) Gibbs-sweep over the lattice */
void gibbs(const size_t n,
           int* state) {
  for (int i=0;i<n;++i) {
    for (int j=0;j<n;++j) {
      int nup = state[LIDX(n,i+1,j)]
        + state[LIDX(n,i-1,j)]
        + state[LIDX(n,i,j+1)]
        + state[LIDX(n,i,j-1)];
      double xi = uniform_dist(engine);
      if (xi < p_gibbs[nup]) {
        state[LIDX(n,i,j)] = 1;
      } else {
        state[LIDX(n,i,j)] = 0;
      }
    }
  }
}

/* Compute magnetisation per spin */
double magnetisation(const size_t n,
                     int* state) {
  double s = 0.0;
  for (int i=0;i<n*n;++i) {
    s+= 2*state[i]-1;
  }
  return s/(n*n);
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
    printf("Usage: %s N BETA\n",argv[0]);
    exit(1);
  }
  n = atoi(argv[1]);
  beta = atof(argv[2]);
  printf("n = %d\n",n);
  printf("beta = %f\n",beta);
  printf("n_samples = %zu\n",n_sample);
  printf("n_burnin = %zu\n",n_burnin);
  
  // Initialise probabilities for Gibbs sampler
  for (int k=0;k<5;++k) {
    double Delta = (-4+2*k) + h;
    p_gibbs[k] = 1./(1.+exp(-2.*beta*Delta));
  }
  // Initialise state
  int* state = (int*) malloc(n*n*sizeof(int));
  std::uniform_int_distribution<int> uniform_int_dist(0, 1);

  for (int ell=0;ell<n*n;++ell) {
    state[ell] = uniform_int_dist(engine);
  }
  // Burnin
  for (int k=0;k<n_burnin;++k) {
    gibbs(n,state);
  }

  // Sampling
  double S_Q = 0.0;
  double S_QQ = 0.0;
  double S_QQQQ = 0.0;
  auto start = std::chrono::high_resolution_clock::now();
  for (int k=1;k<=n_sample;++k) {
    gibbs(n,state);
    double Q = magnetisation(n,state);
    double dS_Q = (Q-S_Q)/k;
    double dS_QQ = (Q*Q-S_QQ)/k;
    double dS_QQQQ = (Q*Q*Q*Q-S_QQQQ)/k;
    S_Q += dS_Q;
    S_QQ += dS_QQ;
    S_QQQQ += dS_QQQQ;
  }
  auto end = std::chrono::high_resolution_clock::now();
  double dS_Q = sqrt((S_QQ-S_Q*S_Q)/(n_sample-1.));
  double dS_QQ = sqrt((S_QQQQ-S_QQ*S_QQ)/(n_sample-1.));
  double chi_t = beta*(S_QQ-S_Q*S_Q);
  double dchi_t = beta*sqrt(dS_Q*dS_Q + 4.*S_QQ*S_QQ*dS_QQ*dS_QQ);
  printf("mu     = %8.4e +/- %8.4e\n",S_Q,dS_Q);
  printf("mu2    = %8.4e +/- %8.4e\n",S_QQ,dS_QQ);
  printf("chi_t  = %8.4e +/- %8.4e\n",chi_t,dchi_t);
  std::chrono::duration<double> elapsed_seconds = end-start;
  printf("total elapsed time: %8.2f s\n",elapsed_seconds.count());
  printf("time per sample: %6.3f ms\n",1.E3*elapsed_seconds.count()/n_sample);
  free(state);
}
