import subprocess
import re
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

#do_run = True
do_run = False

n_list = (4, 8, 12, 16)

if (do_run):
    for n in n_list:
        beta_min = 0.01
        beta_max = 1.0
        mu = []
        dmu = []
        mu2 = []
        dmu2 = []
        chit = []
        dchit = []
        beta = []
        for beta_ in np.arange(beta_min,beta_max,0.02):
            cmd = "./ising.x "+str(n)+" "+str(beta_)
            p = subprocess.Popen(cmd,
                                    shell=True,
                                    encoding="utf-8",
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
            encoding = 'utf-8'
            beta.append(beta_)
            for line in p.stdout.readlines():
                print (line.strip())
                m = re.match(' *mu *= *(.+) *\+\/\- *(.+) *',line)
                if m:
                    mu.append(float(m.group(1)))
                    dmu.append(float(m.group(2)))
                m = re.match(' *mu2 *= *(.+) *\+\/\- *(.+) *',line)
                if m:
                    mu2.append(float(m.group(1)))
                    dmu2.append(float(m.group(2)))
                m = re.match(' *chi_t *= *(.+) *\+\/\- *(.+) *',line)
                if m:
                    chit.append(float(m.group(1)))
                    dchit.append(float(m.group(2)))
        with open("ising_"+str(n)+".csv","w") as f:
            print ("beta , mu, dmu, mu2, dmu2, chit , dchit", file=f) 
            for j in range(len(beta)):
                print ("%12.8f" % beta[j],
                           ", %12.8e" % mu[j],", %12.8e" % dmu[j],
                           ", %12.8e" % mu2[j],", %12.8e" % dmu2[j],
                           ", %12.8e" % chit[j],", %12.8e" % dchit[j],file=f)

quantities = ("mu","mu2","chit")
color=('blue','red','black','green')
label = {"mu":r"magnetisation $\langle\mu\rangle$",
         "mu2":r"squared magnetisation $\langle\mu^2\rangle$",
         "chit":r"susceptibility $\chi_t/N_{\operatorname{spin}}$"}
plt.clf()
fig, axs = plt.subplots(3,1)
for k,Q in enumerate(quantities):
    for j,n in enumerate(n_list):
        print (n)
        data = pd.DataFrame(pd.read_csv("ising_"+str(n)+".csv"))
        X = np.asarray(data.iloc[:,0])
        Y = np.asarray(data.iloc[:,1+2*k])
        dY = np.asarray(data.iloc[:,2+2*k])
        ax = axs[k]
        ax.plot(X,Y,color=color[j],linewidth=2,
                    label=r'lattice =$'+str(n)+r'\times'+str(n)+'$')
        ax.errorbar(X,Y,yerr=dY,
                        color=color[j],
                        elinewidth=2)

        beta_c = 0.5*np.log(1.+np.sqrt(2.))
        ax.set_xlabel(r"coupling $\beta$")
        ax.set_ylabel(label[Q],fontsize=6)
        ax.set_xlim(0,1)
        if (k==0):
            ax.set_ylim(-1,1)
        elif (k==2):
            ax.set_ylim(0,0.8)
        else:
            ax.set_ylim(0,1)
        clabel = r'critical coupling $\beta_c=\frac{\log(1+\sqrt{2})}{2}$' if j==len(n_list)-1 else None
        ax.plot([beta_c,beta_c],[-1,1],
                linewidth=2,
                linestyle='--',
                color='gray',
                label=clabel)

#        ax.set_ylim(0,1)
        if (k==2):
            ax.legend(loc='upper left',fontsize=6)
            
plt.savefig("ising.pdf",bbox_inches='tight')
